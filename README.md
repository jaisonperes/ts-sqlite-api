## TypeScript API with SQLite

#### Install Dependences
```sh
yarn
```
#### Run Knex Migrates to create database
```sh
yarn knex:migrate
```
#### Start server
```sh
yarn start
```