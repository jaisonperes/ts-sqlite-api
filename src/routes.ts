import express from 'express'
import ClassesController from './controlers/ClassesController'
import ConnectionController from './controlers/ConnectionController'

const routes = express.Router()
const classesController = new ClassesController()
const connectionsController = new ConnectionController()

routes.post('/classes', classesController.create)
routes.get('/classes', classesController.index)

routes.get('/connections', connectionsController.index)
routes.post('/connections', connectionsController.create)

export default routes