import Knex from 'knex'

export async function up(knex: Knex) {
  // Changes
  return knex.schema.createTable('connections', table => {
    table.increments('id').primary()

    // Relationship with user
    table.integer('user_id')
      .notNullable()
      .references('id')
      .inTable('users')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')

    table.timestamp('create_at')
      .defaultTo(knex.raw('CURRENT_TIMESTAMP'))
      .notNullable()
  })
}

export async function down(knex: Knex) {
  // Rollback
  return knex.schema.dropTable('connections')
}